<?php
/**
 * @file
 * Theme related functions for the Twistage module.
 */

/**
 * Returns HTML for an video field widget.
 * developed by Debraj
 *
 * @param $variables
 *   An associative array containing:
 *   - element: A render element representing the image field widget.
 *
 * @ingroup themeable
 */

/**
 * Theme function for play order form
 */
function theme_twistage_order_admin_form($variables) {

  $form = $variables['form'];

  $rows = array();  
  $row = array();
  foreach (element_children($form['videos']) as $key) {
        
    $form['videos'][$key]['order']['#attributes']['class'] = array('twistage-order-weight');
    
    $row[] = array(
      'data' => drupal_render($form['videos'][$key]['active']), 'class' => array('draggable')
    );
    $row[] = array(
      'data' => drupal_render($form['videos'][$key]['order']), 'class' => array('draggable')
    );
    $row[] = array(
      'data' => drupal_render($form['videos'][$key]['title']), 'class' => array('draggable')
    );

  $row[] = array(
      'data' => $key, 'class' => array('draggable')
    );
    $rows[] = $row;
  }
  
  $html = drupal_render($form['info']);
  $html .= theme('table', array('header' => $form['#table_header'], 'rows' => $rows, 'attributes' => array('id' => 'twistage-order-table')));
  $html .= drupal_render_children($form);

  drupal_add_tabledrag('twistage-order-table', 'order', 'sibling', 'twistage-order-weight');

  return $html;
}

/**
 * Render the HTML for an embedded Twistage player. This could ultimately be combined functionality-wise with theme_twistage_video in a future version.
 *
 * @param videos
 *   An array of video objects as returned by twistage_fetch_videos.
 *   If the array contains one video, it will be rendered as such. If it contains multiple videos, they will be rendered in "playlist" mode.
 * @return 
 *   string containing the html to embed the player
 */
function theme_twistage_block($pid) {
  if (!$pid) {
    return "";
  }
  
  $profile = twistage_get_profile($pid);
  
  if (!$profile) {
    return "";
  }
  
  drupal_set_html_head('<script type="text/javascript" src="http://service.twistage.com/api/script"></script>');
  $preroll = twistage_get_preroll_video($profile->pid);
  
  $result = db_query("SELECT * FROM {twistage_videos} WHERE play_active = 1 AND pid = :pid AND site_key = :site_key AND availability = :available ORDER BY play_order", array(':pid' => $profile->pid, ':site_key' => $profile->site_key, 'available' => 'available'));
  $videos = array();
  foreach($result as $video) {  
    $videos[] = $video;
  }
  
  return theme('twistage_video', array($videos, $preroll, $profile->preroll_url, $profile->width, $profile->height, $profile->autoplay));
}

/**
 * Render the HTML for one or more videos.
 *
 * @param videos
 *   A single video object, or an array of video objects, as returned by twistage_fetch_videos.
 *   If a single video is passed, it will be rendered as such. If it contains multiple videos, they will be rendered in "playlist" mode.
 * @param preroll
 *   A single video object representing the preroll video (if any) to show before the main payload. Prerolls will have the video controller hidden
 *   (so as to prevent the viewer from skipping it) and will link to the preroll_url parameter.
 * @param preroll_url
 *   A string containing the URL to link to if the viewer clicks on the preroll video.
 * @param width
 *   Pixel width with which to render the player. If none is passed, the values from te profile of the FIRST payload video will be used 
 *   (since the size cannot be changed on the fly)
 * @param height
 *   Pixel height for the player. Same restrictions as the width.
 * @param autoplay
 *    
 * @return 
 *   string containing the html to embed the player
 */
function theme_twistage_video($videos, $preroll = NULL, $preroll_url = NULL, $width = NULL, $height = NULL, $autoplay = NULL) {

  if (empty($videos)) {
    return '';
  }

  if (!is_array($videos)) {
    $videos = array($videos);
  }
  $profile = twistage_get_profile($videos[0]->pid);
  $preroll->hide_controller = FALSE;
  
  if (!$width) {
    $width = $profile->width;
  }
  
  if (!$height) {
    $height = $profile->height;
  }
  
  if (is_null($autoplay)) {
    $autoplay = $profile->autoplay;
  }
      
  if ($autoplay) {
    $apstr = 'config: {autoplay: true},';
  } else {
    $apstr = '';
  }
  
  $add = array();
  
  foreach($videos as $video) {
    if(isset($video->vid) && $video->vid) { //Some times we don't get video vid
      if (!$preroll->hide_controller) {
        $add[] = '{video_id:"' . $video->vid . '"}';
      } else if($preroll->hide_controller) {
        $add[] = '{video_id:"' . $video->vid . '", control_visibility: false, link: "' . urlencode($profile->preroll_url) . '"}';
      }
    }
  }

  
  $vlist = implode(', ', $add);
  
  $html = '<script type="text/javascript" src="http://service.twistage.com/api/script"></script>';
  $html .= '<script type="text/javascript">
      viewList([' . $vlist . '], {
        ' . $apstr . '
        width: '.$width.',
        height: '.$height.',
      });
    </script>';

  return $html;
}

function theme_twistage_formatter_video($item, $entity = NULL, $entity_type = NULL, $field = NULL, $instance = NULL) {
 if(!$item['item']){
  return '';
  }

 
  $video = $item['item'];  
  $profile = twistage_get_profile($video->pid);
  $preroll = twistage_get_preroll_video($profile->pid);
  return theme('twistage_video', array($video), $preroll, $profile->preroll_url, $profile->width, $profile->height, $profile->autoplay);
  
}

/**
 * Theme function to return an HTML table with other videos.
 * I consider this the weakest link in this module, and future releases will hopefully
 * provide a more extensible means of listing other videos.
 */
function theme_twistage_table($pid, $active_vid, $colspan = 3, $thumb_width = 150) {
  drupal_add_css(drupal_get_path('module', 'twistage') . '/twistage.css');
  $profile = twistage_get_profile($pid);
  
  $result = db_query("SELECT * FROM {twistage_videos} WHERE pid = :pid AND site_key = :site_key AND play_active = 1 ORDER BY play_order", array(':pid' => $pid, ':site_key' => $profile->site_key));
  
  $header = array(array(
    'data' => t('<b>More videos from @name...</b>', array('@name' => $profile->description)), 
    'colspan' => $colspan,
  ));
  $rows = array();
  $row = array();
  foreach ($result as $video) {
    $row[] = array(
      'data' => '<div class="twistage-vid" style="display: none;">' . $video->vid . '</div><img src="http://service.twistage.com/screenshots/' . $video->vid . '?width=' . $thumb_width . '" class="twistage-thumb"><p style="padding:0px; text-align:center;">' . $video->title . '</p>',
      'class' => ($video->vid == $active_vid ? 'twistage-cell-activate playing' : 'twistage-cell-activate'),
    );
    if (count($row) == $colspan) {
      $rows[] = $row;
      $row = array();
    }
  }
  
  if ($row) {
    $rows[] = $row;
  }
  
  if ($rows) {
    return theme('table', $header, $rows, array('class' => 'twistage-table'));
  } else {
    return "";
  }
}
