function play(vid, auto) {
  if(Drupal.settings.twistage['preroll'] != null) {
    list = [{video_id: Drupal.settings.twistage['preroll'], control_visibility: false, link: Drupal.settings.twistage['preroll_url']}, {video_id: vid}];
  } else {
    list = [{video_id: vid}];
  }
  params = {width: Drupal.settings.twistage['width'], height: Drupal.settings.twistage['height'], config: {autoplay: auto}};
  $('#twistage-player').html(getPlaylistViewer(list, params));
}

$(document).ready(function() {
  play(Drupal.settings.twistage['vid'], false);
  $('.twistage-cell-activate').click(function() {
    vid = $(this).find('.twistage-vid').text();
    $('.twistage-cell-activate').removeClass('playing');
    $(this).addClass('playing');
    play(vid, true);
  });
});